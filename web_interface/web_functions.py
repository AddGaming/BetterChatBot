import datetime

from flask_login import current_user
from db_models import Application, Log
from web_interface import DATABASE


def lightswitch(mode):
    if mode is not None:
        current_user.light_mode = bool(int(mode))
        # TODO: Catch invalid input


def nlu_switch(mode):
    if mode is not None:
        # TODO: Catch invalid input
        if Application.query.all():
            app = Application.query.get(1)
        else:
            app = Application()
            DATABASE.session.add(app)
        app.nlu = bool(int(mode))
        DATABASE.session.commit()
        log(text=f"NLU-Status set to {bool(int(mode))}", category="Status")


def log(text="", category="", platform=4):
    DATABASE.session.add(Log(date=datetime.datetime.now(), platform=platform, category=category, text=text))
    DATABASE.session.commit()
