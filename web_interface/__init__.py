from flask import Flask
from flask_login import LoginManager
from flask_sqlalchemy import SQLAlchemy
import hashlib
import time

from database import create_database

DATABASE = SQLAlchemy()
DB_NAME = "database.db"
# TODO: This looks ugly. how 2 better do this?
try:
    from NLU.Dialog_flow_manager import DialogFlowManager
    from db_models import GoogleSetting
    DIALOGFLOW = DialogFlowManager(GoogleSetting.query.filter_by(id=1).first().project_id)
except:
    pass


def create_app():
    # App initialisation
    flask_app = Flask(__name__)
    flask_app.config['SECRET_KEY'] = str(hashlib.sha256(str(time.localtime()).encode()).hexdigest())

    # setting up routes
    from .routes import routes
    flask_app.register_blueprint(routes, url_prefix="/")

    # database setup
    flask_app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False  # disabling to suppress error
    flask_app.config['SQLALCHEMY_DATABASE_URI'] = f'sqlite:///{DB_NAME}'
    DATABASE.init_app(flask_app)
    # imports before db creation - else errors
    from db_models import User, Ott, Application, Platform, Log, Group, Command, Entity, Word, Answer, Question, \
        GoogleSetting
    create_database(flask_app=flask_app, db=DATABASE, db_name=DB_NAME)

    # account management
    login_manager = LoginManager()
    login_manager.login_view = "routes.login"
    login_manager.init_app(flask_app)

    @login_manager.user_loader
    def load_user(u_id):
        return User.query.get(int(u_id))

    return flask_app
