import time

import bcrypt
import hashlib
import base64
import datetime


def hash_pw(raw_pw: str):
    enc_pw = raw_pw.encode("utf-8")
    hashed_pw = str(base64.b64encode(hashlib.sha256(enc_pw).digest()))  # doing this to accept pw of any length
    enc_pw = hashed_pw.encode("utf-8")
    hashed_pw = bcrypt.hashpw(enc_pw, bcrypt.gensalt(15))
    return hashed_pw.decode()


def check_pw(raw_pw: str, db_hash: str):
    from web_interface.web_functions import log
    enc_pw = raw_pw.encode("utf-8")
    hashed_pw = str(base64.b64encode(hashlib.sha256(enc_pw).digest()))  # doing this to accept pw of any length
    enc_pw = hashed_pw.encode("utf-8")
    enc_db_hash = db_hash.encode("utf-8")
    if bcrypt.checkpw(enc_pw, enc_db_hash):
        # log(text="Password check successful", category="Status")
        return True
    else:
        # log(text="Password check failed", category="Status")
        return False


def time_hash(t=None):
    """
    gives you the sha256 of the time you passed in\n
    :param t: (optional) datetime obj
    :return: str
    """
    if t is None:
        time.sleep(0.01)  # putting this in here to prevent equivalent results upon consecutive uses
        t = datetime.datetime.now()
    t = str(t)
    h = t.encode("utf-8")
    h = str(base64.b64encode(hashlib.sha256(h).digest()).decode())
    return h


if __name__ == "__main__":
    # h1 = hash_pw("hi"*100)
    # print(check_pw("hi"*100, h1))
    print(time_hash())
    print(time_hash())
