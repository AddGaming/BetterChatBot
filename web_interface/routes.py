# the collection of all routes that the flask app can/should use
from flask import Blueprint, render_template, request, flash, redirect, url_for
from flask_login import login_user, logout_user, login_required, current_user

from database import init_db, top_commands_3, flop_commands_3, call_analysis, parse_csv_to_commands, update_command, \
    parse_csv_to_questions, parse_csv_to_answers, parse_csv_to_entities, parse_csv_to_words, update_nlu_word, \
    update_nlu_question, update_application, update_google_settings
from db_models import User, Ott, Platform, Application, Log, Command, Group, Answer, Question, Entity, Word
from web_interface import DATABASE
from web_interface.crypto import hash_pw, check_pw, time_hash
from web_interface.web_functions import lightswitch, nlu_switch, log

routes = Blueprint("routes", __name__)


@routes.route("/")
@routes.route("/home")
@login_required  # make sure this decorator is at the bottom - else doesn't work
def home():
    if request.method == "GET":
        lightswitch(request.args.get("Light"))
        nlu_switch(request.args.get("set_NLU"))

    update_application(DATABASE)
    call_ana = call_analysis()

    from db_models import Application
    app = Application.query.filter_by(id=1).first()
    nlu_stats = {"Word_count": app.nlu_word_count,
                 "Intent_count": app.nlu_intent_count,
                 "Entity_count": app.nlu_entity_count,
                 "Uncategorized_word_count": app.nlu_to_do_entities,
                 "Categorized_word_count": app.nlu_word_count - app.nlu_to_do_entities,
                 "Training_sentence_count": app.nlu_training_count,
                 "Uncategorized_sentence_count": app.nlu_to_do_intents}

    try:
        percentages = {
            "Discord": call_ana["discord"][3],
            "Twitch": call_ana["twitch"][3],
            "Telegram": call_ana["telegram"][3],
            "Youtube": call_ana["youtube"][3],
            "categorized_word_count": nlu_stats['Categorized_word_count'] /
                                      ((nlu_stats['Uncategorized_word_count'] + nlu_stats[
                                          'Categorized_word_count']) / 100),
            "uncategorized_word_count": nlu_stats['Uncategorized_word_count'] /
                                        ((nlu_stats['Uncategorized_word_count'] + nlu_stats[
                                            'Categorized_word_count']) / 100),
            "training_sentence": nlu_stats['Training_sentence_count'] /
                                 ((nlu_stats['Uncategorized_sentence_count'] + nlu_stats[
                                     'Training_sentence_count']) / 100),
            "uncategorized_sentence_count": nlu_stats['Uncategorized_sentence_count'] /
                                            ((nlu_stats['Uncategorized_sentence_count'] + nlu_stats[
                                                'Training_sentence_count']) / 100)
        }
    except ZeroDivisionError:
        percentages = {
            "Discord": call_ana["discord"][3],
            "Twitch": call_ana["twitch"][3],
            "Telegram": call_ana["telegram"][3],
            "Youtube": call_ana["youtube"][3],
            "categorized_word_count": 0, "uncategorized_word_count": 0, "training_sentence": 0,
            "uncategorized_sentence_count": 0
        }

    platform_status = {"Discord": Platform.query.get(1).status,
                       "Twitch": Platform.query.get(4).status,
                       "Telegram": Platform.query.get(2).status,
                       "Youtube": Platform.query.get(3).status}

    log(text="Rendered \"/home\"", category="Web")
    return render_template(
        "home.html", percentages=percentages,
        platform_status=platform_status, nlu_stats=nlu_stats, user=current_user, call_ana=call_ana,
        flops=flop_commands_3(DATABASE), top=top_commands_3(DATABASE), app=app)


@routes.route("/login", methods=["POST", "GET"])
def login():
    if request.method == "GET":
        lightswitch(request.args.get("Light"))
    if request.method == "POST":
        mail = request.form.get("mail")
        pw = request.form.get("pw")

        user = User.query.filter_by(mail=mail).first()
        if user:
            if check_pw(pw, user.pw):
                login_user(user, remember=True)
                log(text=f"Logged in User {current_user.mail}", category="Status")
                log(text="Redirecting to \"/home\"", category="Web")
                return redirect(url_for("routes.home"))
            else:
                log(text=f"Login failed from {mail}", category="Status")
                flash("Email or Password is wrong", category="error")
        else:
            log(text=f"Login failed from {mail}", category="Status")
            flash("Email or Password is wrong", category="error")

    log(text="Rendered \"/login\"", category="Web")
    return render_template("login.html", user=current_user, app=Application.query.get(1))


@routes.route("/logout")
@login_required
def logout():
    log(text=f"User {current_user.mail} logged out", category="Status")
    logout_user()
    return redirect(url_for("routes.login"))


@routes.route("/register", methods=["POST", "GET"])
def register():
    if request.method == "GET":
        lightswitch(request.args.get("Light"))
    if request.method == "POST":
        mail = request.form.get("mail")
        pw1 = request.form.get("pw1")
        pw2 = request.form.get("pw2")
        ott = request.form.get("ott")

        unique = not User.query.filter_by(mail=mail).first()
        ott_valid = not not Ott.query.filter_by(value=ott).first()
        if not ott_valid:
            if not Ott.query.all():
                init_db(DATABASE)
                ott = Ott.query.get(1).value
                print("otts creates")
            else:
                flash("OTT is invalid", category="error")
        if not unique or len(mail) < 4:
            flash("Email is already in use or to short (>4 characters)", category="error")
        elif pw1 != pw2:
            flash("Passwords dont match", category="error")
        elif len(pw1) < 8:
            flash("Password is to short. It should be at least 8 characters long!", category="error")
        else:
            new_user = User(mail=mail, pw=hash_pw(pw1))
            DATABASE.session.add(new_user)
            DATABASE.session.commit()
            # refreshing used ott
            # TODO: sqlalchemy.orm.exc.UnmappedInstanceError: Class 'builtins.NoneType' is not mapped
            DATABASE.session.delete(Ott.query.filter_by(value=ott).first())
            DATABASE.session.add(Ott(value=time_hash()))
            DATABASE.session.commit()
            login_user(new_user, remember=True)
            return redirect(url_for("routes.home"))

    return render_template("register.html", user=current_user, app=Application.query.get(1))


@routes.route("/profile", methods=["POST", "GET"])
@login_required
def profile():
    if request.method == "GET":
        lightswitch(request.args.get("Light"))
    if request.method == "POST":
        if request.form.get("new-mail"):  # mail update
            new_mail = request.form.get("new-mail")
            if len(new_mail) > 4:
                User.query.filter_by(id=current_user.id).first().mail = new_mail
            else:
                flash("Mail address is to short", category="error")
        elif request.form.get("old-pw"):
            old_pw = request.form.get("old-pw")
            new_pw1 = request.form.get("new-pw1")
            new_pw2 = request.form.get("new-pw2")
            if not check_pw(old_pw, current_user.pw):
                flash("Your current Password and the Password you entered do not match", category="error")
            elif new_pw1 != new_pw2:
                flash("The new Passwords you entered aren't identical", category="error")
            else:
                User.query.filter_by(id=current_user.id).first().pw = hash_pw(new_pw1)
        else:
            flash("You submitted an empty form", category="error")
        DATABASE.session.commit()

    return render_template("profile.html", user=current_user, app=Application.query.get(1))


@routes.route("/otts")
@login_required
def otts():
    if request.method == "GET":
        lightswitch(request.args.get("Light"))

    ott_data = Ott.query.all()

    return render_template("otts.html", otts=ott_data, user=current_user, app=Application.query.get(1))


@routes.route("/log-view")
@login_required
def log_view():
    if request.method == "GET":
        lightswitch(request.args.get("Light"))

    from sqlalchemy import desc
    log_data = DATABASE.session.query(Log).order_by(desc(Log.date))

    return render_template("log_view.html", logs=log_data, user=current_user, app=Application.query.get(1))


@routes.route("/dashboard")
@routes.route("/command-dashboard")
@login_required
def command_dashboard():
    if request.method == "GET":
        lightswitch(request.args.get("Light"))

    call_ana = call_analysis()

    return render_template(
        "command_dashboard.html", call_ana=call_ana, user=current_user, flops=flop_commands_3(DATABASE),
        top=top_commands_3(DATABASE), app=Application.query.get(1))


@routes.route("/command-view", methods=["GET", "POST"])
@routes.route("/command", methods=["GET", "POST"])
@login_required
def command_view():
    # TODO: Import stats from db
    command_id = 1
    if request.method == "GET":
        lightswitch(request.args.get("Light"))
        # TODO: Catch invalid id's
        try:
            command_id = int(request.args.get("id"))
        except:
            command_id = 1
    elif request.method == "POST":
        command_id = request.form.get("current_id")
        if request.form.get("del_flag"):
            Command.query.filter_by(id=command_id).delete()
            DATABASE.session.commit()
            redirect(url_for("routes.command_table"))
        else:
            update_data = [
                request.form.get("command_name"),
                request.form.get("command_symbol"),
                int(request.form.get("group_id")),
                bool(request.form.get("cmd_active")),
                request.form.get("text_discord"),
                request.form.get("text_telegram"),
                request.form.get("text_youtube"),
                request.form.get("text_twitch"),
                bool(request.form.get("cmd_auto_delete")),
                int(request.form.get("cmd_delete_time"))
            ]
            print(update_data)
            update_command(data=update_data, db=DATABASE)

    command = Command.query.filter_by(id=command_id).first()
    from sqlalchemy import desc
    rankings = DATABASE.session.query(Command).order_by(desc(Command.usages))
    counter = 0
    rank = -1  # vs compiler warning
    for c in rankings:
        if c.id == command.id:
            rank = counter
            break
        else:
            counter += 1
    groups = Group.query.all()
    author = User.query.filter_by(id=command.author).first()

    return render_template("command_view.html", id=command_id, command=command, ranking=rank,
                           groups=groups, author=author, user=current_user, app=Application.query.get(1))


@routes.route("/platform-overview")
@login_required
def platform_overview():
    if request.method == "GET":
        lightswitch(request.args.get("Light"))

    # TODO: replace with real data from db
    # DEMO DATA START

    call_ana = call_analysis()

    platform_status = {"Discord": Platform.query.filter_by(name="discord").first().status,
                       "Twitch": Platform.query.filter_by(name="discord").first().status,
                       "Telegram": Platform.query.filter_by(name="discord").first().status,
                       "Youtube": Platform.query.filter_by(name="discord").first().status}
    calls_today = {"Discord_commands": 7,
                   "Twitch_commands": 2,
                   "Telegram_commands": 0,
                   "Youtube_commands": 1,
                   "Discord_nlu": 4,
                   "Twitch_nlu": 0,
                   "Telegram_nlu": 0,
                   "Youtube_nlu": 0}
    calls_week = {"Discord_commands": 70,
                  "Twitch_commands": 20,
                  "Telegram_commands": 0,
                  "Youtube_commands": 5,
                  "Discord_nlu": 45,
                  "Twitch_nlu": 2,
                  "Telegram_nlu": 5,
                  "Youtube_nlu": 0}

    # DEMO DATA SEND
    # return render_template("coming_soon.html", user=current_user, app=Application.query.get(1))
    # TODO: finish and then activate this
    return render_template(
        "platform_overview.html", platform_status=platform_status, calls_today=calls_today, calls_week=calls_week,
        user=current_user, app=Application.query.get(1))


@routes.route("/command-table", methods=["POST", "GET"])
@login_required
def command_table():
    if request.method == "GET":
        lightswitch(request.args.get("Light"))
    elif request.method == "POST":
        update_command(data=[
            request.form.get("cmd_name"),
            request.form.get("cmd_symbol"),
            int(request.form.get("cmd_group")),
            not not request.form.get("cmd_active"),
            request.form.get("cmd_discord"),
            request.form.get("cmd_telegram"),
            request.form.get("cmd_youtube"),
            request.form.get("cmd_twitch"),
            not not request.form.get("cmd_auto_delete"),
            None
        ], db=DATABASE)

    # DEMO DATA START
    groups = Group.query.all()
    activated_commands = []
    deactivated_commands = []
    for command in Command.query.all():
        if command.active:
            activated_commands.append(command)
        else:
            deactivated_commands.append(command)

    return render_template("command_table.html", activated_commands=activated_commands,
                           deactivated_commands=deactivated_commands, groups=groups, user=current_user,
                           authors=User.query.all(), app=Application.query.get(1))


@routes.route("/command-input", methods=["POST", "GET"])
@routes.route("/command-add", methods=["POST", "GET"])
@login_required
def command_input():
    if request.method == "GET":
        lightswitch(request.args.get("Light"))
    if request.method == "POST":
        csv = request.form.get("csv")
        parse_csv_to_commands(csv=csv, db=DATABASE)

    return render_template("command_inport.html", user=current_user, app=Application.query.get(1))


@routes.route("/timer-table")
@login_required
def timer_table():
    if request.method == "GET":
        lightswitch(request.args.get("Light"))

    return render_template("coming_soon.html", user=current_user, app=Application.query.get(1))


@routes.route("/command-groups")
@login_required
def command_groups():
    if request.method == "GET":
        lightswitch(request.args.get("Light"))

    return render_template("coming_soon.html", user=current_user, app=Application.query.get(1))


@routes.route("/nlu-overview", methods=["GET", "POST"])
@login_required
def nlu_overview():
    if request.method == "GET":
        lightswitch(request.args.get("Light"))
        nlu_switch(request.args.get("set_NLU"))
    if request.method == "POST":
        if not not request.form.get("key_type"):
            update_data = [request.form.get("key_type"), request.form.get("key_project_id"),
                           request.form.get("key_private_key_id"),
                           request.form.get("key_private_key").replace("\\", "\\\\").replace("\r", ""),
                           request.form.get("key_client_email"), request.form.get("key_client_id"),
                           request.form.get("key_auth_uri"), request.form.get("key_token_uri"),
                           request.form.get("key_auth_provider_x509_cert_url"),
                           request.form.get("key_client_x509_cert_url")]
            all_valid = True
            for e in update_data:
                if e is None:
                    all_valid = False
            if all_valid:
                update_google_settings(update_data, DATABASE)
            else:
                flash("At leased one of the input fields is empty", "error")
        elif not not request.form.get("train"):
            flash("The bot starts training. ready in 10 min", "success")

    update_application(DATABASE)
    call_ana = call_analysis()
    calls = {"Discord_commands": call_ana["discord"][2][0],
             "Twitch_commands": call_ana["twitch"][2][0],
             "Telegram_commands": call_ana["telegram"][2][0],
             "Youtube_commands": call_ana["youtube"][2][0],
             "Discord_nlu": call_ana["discord"][2][1],
             "Twitch_nlu": call_ana["twitch"][2][1],
             "Telegram_nlu": call_ana["telegram"][2][1],
             "Youtube_nlu": call_ana["youtube"][2][1]}

    from db_models import Application, GoogleSetting
    app = Application.query.filter_by(id=1).first()
    nlu_stats = {"Word_count": app.nlu_word_count,
                 "Intent_count": app.nlu_intent_count,
                 "Entity_count": app.nlu_entity_count,
                 "Uncategorized_word_count": app.nlu_to_do_entities,
                 "Categorized_word_count": app.nlu_word_count - app.nlu_to_do_entities,
                 "Training_sentence_count": app.nlu_training_count,
                 "Uncategorized_sentence_count": app.nlu_to_do_intents}

    nlu_calls = call_ana["total_nlu"]
    command_calls = call_ana["total_commands"]

    try:
        # TODO: implement 3Satz and use it here
        percentages = {
            "nlu_calls": nlu_calls * (100 / (command_calls + nlu_calls)),
            "command_calls": command_calls * (100 / (command_calls + nlu_calls)),
            "training_sentence": nlu_stats['Training_sentence_count'] *
                                 (100 / (nlu_stats['Uncategorized_sentence_count'] + nlu_stats[
                                     'Training_sentence_count'])),
            "categorized_word_count": nlu_stats['Categorized_word_count'] /
                                      ((nlu_stats['Uncategorized_word_count'] + nlu_stats[
                                          'Categorized_word_count']) / 100),
            "uncategorized_word_count": nlu_stats['Uncategorized_word_count'] /
                                        ((nlu_stats['Uncategorized_word_count'] + nlu_stats[
                                            'Categorized_word_count']) / 100),
            "uncategorized_sentence_count": nlu_stats['Uncategorized_sentence_count'] /
                                            ((nlu_stats['Uncategorized_sentence_count'] + nlu_stats[
                                                'Training_sentence_count']) / 100),
            "discord_nlu": calls['Discord_nlu'] * (100 / (calls['Discord_nlu'] + calls['Discord_commands'])),
            "discord_commands": calls['Discord_commands'] * (100 / (calls['Discord_nlu'] + calls['Discord_commands'])),
            "telegram_nlu": calls['Telegram_nlu'] / ((calls['Telegram_nlu'] + calls['Telegram_commands']) / 100),
            "telegram_commands": calls['Telegram_commands'] / (
                    (calls['Telegram_nlu'] + calls['Telegram_commands']) / 100),
            "youtube_nlu": calls['Youtube_nlu'] / ((calls['Youtube_nlu'] + calls['Youtube_commands']) / 100),
            "youtube_commands": calls['Youtube_commands'] / ((calls['Youtube_nlu'] + calls['Youtube_commands']) / 100),
            "twitch_nlu": calls['Twitch_nlu'] / ((calls['Twitch_nlu'] + calls['Twitch_commands']) / 100),
            "twitch_commands": calls['Twitch_commands'] / ((calls['Twitch_nlu'] + calls['Twitch_commands']) / 100)
        }
    except ZeroDivisionError:
        percentages = {
            "nlu_calls": 0, "command_calls": 0, "training_sentence": 0, "categorized_word_count": 0,
            "uncategorized_word_count": 0, "uncategorized_sentence_count": 0, "discord_nlu": 0,
            "discord_commands": 0, "telegram_nlu": 0, "telegram_commands": 0, "youtube_nlu": 0,
            "youtube_commands": 0, "twitch_nlu": 0, "twitch_commands": 0
        }

    connection_status = GoogleSetting.query.filter_by(id=1).first().connected

    return render_template(
        "nlu_overview.html", nlu_stats=nlu_stats, calls=calls, nlu_calls=nlu_calls, command_calls=command_calls,
        percentages=percentages, call_ana=call_ana, user=current_user, app=Application.query.get(1),
        connection_status=connection_status)


@routes.route("/nlu-entities", methods=["GET", "POST"])
@login_required
def nlu_entities():
    if request.method == "GET":
        lightswitch(request.args.get("Light"))

    if request.method == "POST":
        word_text = request.form.get("word_text")
        word_status = request.form.get("word_status")
        word_parent = request.form.get("word_parent")  # as id
        word_parent_input = request.form.get("word_parent_input")
        print([word_text, word_status, word_parent, word_parent_input])
        if word_parent_input:
            exists = Entity.query.filter_by(name=word_parent_input).first()
            if exists:
                update_nlu_word([word_text, word_status, exists.id], DATABASE)
                if word_status == "ignore" or word_status == "categorized":
                    Application.query.filter_by(id=1).first().nlu_to_do_entities -= 1
                else:
                    Application.query.filter_by(id=1).first().nlu_to_do_intents += 1
                DATABASE.session.commit()
            else:
                new_entity = Entity(name=word_parent_input)
                DATABASE.session.add(new_entity)
                DATABASE.session.commit()
                update_nlu_word([word_text, word_status, Entity.query.filter_by(name=word_parent_input).first().id],
                                DATABASE)
                if word_status == "ignore" or word_status == "categorized":
                    Application.query.filter_by(id=1).first().nlu_to_do_entities -= 1
                else:
                    Application.query.filter_by(id=1).first().nlu_to_do_intents += 1
                DATABASE.session.commit()
        else:
            update_nlu_word([word_text, word_status, word_parent], DATABASE)
            if word_status == "ignore" or word_status == "categorized":
                Application.query.filter_by(id=1).first().nlu_to_do_entities -= 1
            else:
                Application.query.filter_by(id=1).first().nlu_to_do_intents += 1
            DATABASE.session.commit()

    # Page-prep
    entities = Entity.query.all()
    words = Word.query.all()
    new_words = []
    old_words = []
    for w in words:
        if w.status == "to_do" or w.status == "uncategorized":
            new_words.append(w)
        else:
            old_words.append(w)

    return render_template(
        "nlu_entities.html", new_words=new_words, old_words=old_words, entities=entities, user=current_user,
        app=Application.query.get(1))


@routes.route("/nlu-intents", methods=["GET", "POST"])
@login_required
def nlu_intents():
    if request.method == "GET":
        lightswitch(request.args.get("Light"))

    # TODO: implement post acceptance
    if request.method == "POST":
        text = request.form.get("question_text")
        status = request.form.get("question_status")
        answer_id = request.form.get("answer_id")
        new_answer_text = request.form.get("new_answer")
        print([text, status, answer_id, new_answer_text])
        if new_answer_text:
            exists = Answer.query.filter_by(text=new_answer_text).first()
            if exists:
                update_nlu_question([text, status, exists.id], DATABASE)
                if status == "ignore" or status == "categorized":
                    Application.query.filter_by(id=1).first().nlu_to_do_intents -= 1
                else:
                    Application.query.filter_by(id=1).first().nlu_to_do_intents += 1
                DATABASE.session.commit()
            else:
                new_answer = Answer(text=new_answer_text, author=current_user.id)
                DATABASE.session.add(new_answer)
                if status == "ignore" or status == "categorized":
                    Application.query.filter_by(id=1).first().nlu_to_do_intents -= 1
                else:
                    Application.query.filter_by(id=1).first().nlu_to_do_intents += 1
                DATABASE.session.commit()
                update_nlu_question([text, status, Answer.query.filter_by(text=new_answer_text).first().id], DATABASE)
        else:
            if status == "ignore" or status == "categorized":  # TODO: make this ifelse a function
                Application.query.filter_by(id=1).first().nlu_to_do_intents -= 1
            else:
                Application.query.filter_by(id=1).first().nlu_to_do_intents += 1
            DATABASE.session.commit()
            update_nlu_question([text, status, answer_id], DATABASE)

    answers = Answer.query.all()
    questions = Question.query.all()
    new_questions = []
    training_phrases = []
    ignored_phrases = []
    for q in questions:
        if q.status == "to_do" or q.status == "uncategorized":
            new_questions.append(q)
        elif q.status == "categorized":
            training_phrases.append(q)
        else:
            ignored_phrases.append(q)

    return render_template(
        "nlu_intents.html", answers=answers, new_questions=new_questions, training_phrases=training_phrases,
        ignored_phrases=ignored_phrases, user=current_user, app=Application.query.get(1))


@routes.route("/nlu-import", methods=["POST", "GET"])
@login_required
def nlu_import():
    if request.method == "GET":
        lightswitch(request.args.get("Light"))
    if request.method == "POST":
        if request.form.get("csv-questions"):
            parse_csv_to_questions(csv=request.form.get("csv-questions"), db=DATABASE)
            flash("Questions added to Database", "success")
        elif request.form.get("csv-answers"):
            parse_csv_to_answers(csv=request.form.get("csv-answers"), db=DATABASE)
            flash("Answers added to Database", "success")
        elif request.form.get("csv-entities"):
            parse_csv_to_entities(csv=request.form.get("csv-entities"), db=DATABASE)
            flash("Entities added to Database", "success")
        elif request.form.get("csv-words"):
            parse_csv_to_words(csv=request.form.get("csv-words"), db=DATABASE)
            flash("Words added to Database", "success")
        else:
            flash("You submitted an empty form", category="error")

    return render_template("nlu_import.html", user=current_user, app=Application.query.get(1))


@routes.route("/impressum")
def impressum():
    if request.method == "GET":
        lightswitch(request.args.get("Light"))

    return render_template("impressum.html", user=current_user, app=Application.query.get(1))


@routes.route("/resources")
def resources():
    if request.method == "GET":
        lightswitch(request.args.get("Light"))

    return render_template("resources.html", user=current_user, app=Application.query.get(1))


@routes.route("/chat")
@routes.route("/chat-test")
def chat():
    if request.method == "GET":
        lightswitch(request.args.get("Light"))

    return render_template("chat.html", user=current_user, app=Application.query.get(1))
