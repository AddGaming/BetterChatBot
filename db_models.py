from sqlalchemy.sql import func
from web_interface import DATABASE
from flask_login import UserMixin


class User(DATABASE.Model, UserMixin):
    __tablename__ = "users"
    id = DATABASE.Column(DATABASE.Integer, primary_key=True)
    mail = DATABASE.Column(DATABASE.Text, unique=True, nullable=False)
    pw = DATABASE.Column(DATABASE.Text, nullable=False)
    birthday = DATABASE.Column(DATABASE.DateTime(timezone=True), default=func.now())  # the date the user is created
    changes = DATABASE.Column(DATABASE.Integer, default=0)
    light_mode = DATABASE.Column(DATABASE.Integer, default=0)


class Ott(DATABASE.Model):
    __tablename__ = "otts"
    id = DATABASE.Column(DATABASE.Integer, primary_key=True)
    value = DATABASE.Column(DATABASE.Text, nullable=False)


class Application(DATABASE.Model):
    __tablename__ = "application"
    id = DATABASE.Column(DATABASE.Integer, primary_key=True)
    nlu = DATABASE.Column(DATABASE.Boolean, default=False)
    nlu_to_do_entities = DATABASE.Column(DATABASE.Integer, default=0)
    nlu_to_do_intents = DATABASE.Column(DATABASE.Integer, default=0)
    nlu_word_count = DATABASE.Column(DATABASE.Integer, default=0)
    nlu_intent_count = DATABASE.Column(DATABASE.Integer, default=0)
    nlu_entity_count = DATABASE.Column(DATABASE.Integer, default=0)
    nlu_training_count = DATABASE.Column(DATABASE.Integer, default=0)


class Platform(DATABASE.Model):
    __tablename__ = "platforms"
    id = DATABASE.Column(DATABASE.Integer, primary_key=True)
    name = DATABASE.Column(DATABASE.Text, nullable=False)
    status = DATABASE.Column(DATABASE.Text, default="disconnected")
    n_commands_today = DATABASE.Column(DATABASE.Integer, default=0)
    n_commands_week = DATABASE.Column(DATABASE.Integer, default=0)
    n_commands_total = DATABASE.Column(DATABASE.Integer, default=0)
    n_nlu_today = DATABASE.Column(DATABASE.Integer, default=0)
    n_nlu_week = DATABASE.Column(DATABASE.Integer, default=0)
    n_nlu_total = DATABASE.Column(DATABASE.Integer, default=0)
    app_id = DATABASE.Column(DATABASE.Text, default="")
    public_key = DATABASE.Column(DATABASE.Text, default="")
    permission_lv = DATABASE.Column(DATABASE.Text, default="")
    secret_key = DATABASE.Column(DATABASE.Text, default="")
    logs = DATABASE.relationship("Log", backref="platforms")


class Log(DATABASE.Model):
    """Log(date=, platform=, category="", text="")"""
    __tablename__ = "logs"
    id = DATABASE.Column(DATABASE.Integer, primary_key=True)
    date = DATABASE.Column(DATABASE.DateTime(timezone=True), nullable=False)
    platform = DATABASE.Column(DATABASE.Integer, DATABASE.ForeignKey("platforms.id"), nullable=False)
    category = DATABASE.Column(DATABASE.String(30), nullable=False)
    text = DATABASE.Column(DATABASE.Text, default="")


class Group(DATABASE.Model):
    """Group(name=)"""
    __tablename__ = "groups"
    id = DATABASE.Column(DATABASE.Integer, primary_key=True)
    name = DATABASE.Column(DATABASE.Text, nullable=False)
    member = DATABASE.relationship("Command", backref="groups")
    symbol = DATABASE.Column(DATABASE.String(5), default="/")
    active = DATABASE.Column(DATABASE.Boolean, default=True, nullable=False)


class Command(DATABASE.Model):
    """Command(name=, group=, author=, date=, text_discord=)"""
    __tablename__ = "commands"
    id = DATABASE.Column(DATABASE.Integer, primary_key=True)
    name = DATABASE.Column(DATABASE.Text, nullable=False)
    symbol = DATABASE.Column(DATABASE.String(5), default="/")
    group = DATABASE.Column(DATABASE.Integer, DATABASE.ForeignKey("groups.id"), nullable=False)
    author = DATABASE.Column(DATABASE.Integer, DATABASE.ForeignKey("users.id"), nullable=False)
    date = DATABASE.Column(DATABASE.DateTime(timezone=True), nullable=False)
    usages = DATABASE.Column(DATABASE.Integer, default=0)
    active = DATABASE.Column(DATABASE.Boolean, default=True, nullable=False)
    text_discord = DATABASE.Column(DATABASE.Text, default="")
    text_telegram = DATABASE.Column(DATABASE.Text, default="")
    text_youtube = DATABASE.Column(DATABASE.Text, default="")
    text_twitch = DATABASE.Column(DATABASE.Text, default="")
    auto_delete = DATABASE.Column(DATABASE.Boolean, default=False)
    auto_delete_time = DATABASE.Column(DATABASE.Integer, default=60)


class GoogleSetting(DATABASE.Model):
    """"""
    __tablename__ = "google_settings"
    id = DATABASE.Column(DATABASE.Integer, primary_key=True)
    connected = DATABASE.Column(DATABASE.Boolean, default=False)
    type = DATABASE.Column(DATABASE.Text, default="")
    project_id = DATABASE.Column(DATABASE.Text, default="")
    private_key_id = DATABASE.Column(DATABASE.Text, default="")
    private_key = DATABASE.Column(DATABASE.Text, default="")
    client_mail = DATABASE.Column(DATABASE.Text, default="")
    client_id = DATABASE.Column(DATABASE.Text, default="")
    auth_uri = DATABASE.Column(DATABASE.Text, default="")
    token_uri = DATABASE.Column(DATABASE.Text, default="")
    auth_provider_x509_cert_url = DATABASE.Column(DATABASE.Text, default="")
    client_x509_cert_url = DATABASE.Column(DATABASE.Text, default="")


class Question(DATABASE.Model):
    """Intent(text=, status=, answer=)"""
    __tablename__ = "NLU_Questions"
    id = DATABASE.Column(DATABASE.Integer, primary_key=True)
    text = DATABASE.Column(DATABASE.Text, nullable=False)
    status = DATABASE.Column(DATABASE.String(30), nullable=False)
    answer = DATABASE.Column(DATABASE.Integer, DATABASE.ForeignKey("NLU_Answers.id"), nullable=True)


class Answer(DATABASE.Model):
    """Answer(text=, author=)"""
    __tablename__ = "NLU_Answers"
    id = DATABASE.Column(DATABASE.Integer, primary_key=True)
    text = DATABASE.Column(DATABASE.Text, nullable=False)
    author = DATABASE.Column(DATABASE.Integer, DATABASE.ForeignKey("users.id"), nullable=False)
    questions = DATABASE.relationship("Question", backref="NLU_Answers")


class Word(DATABASE.Model):
    """Word(text=, status=, entity=)"""
    __tablename__ = "NLU_Words"
    id = DATABASE.Column(DATABASE.Integer, primary_key=True)
    text = DATABASE.Column(DATABASE.Text, nullable=False)
    status = DATABASE.Column(DATABASE.String(30), nullable=False)  # "categorized", "ignore", "to_do"/"uncategorized"
    entity = DATABASE.Column(DATABASE.Integer, DATABASE.ForeignKey("NLU_Entities.id"))


class Entity(DATABASE.Model):
    """Entity(name=, url=)"""
    __tablename__ = "NLU_Entities"
    id = DATABASE.Column(DATABASE.Integer, primary_key=True)
    name = DATABASE.Column(DATABASE.Text, nullable=False, unique=True)
    url = DATABASE.Column(DATABASE.Text, default="", unique=True)
    entities = DATABASE.relationship("Word", backref="NLU_Entities")
