import unittest


class TestHashPw(unittest.TestCase):

    def test_2_hashes_of_the_same_are_salted(self):
        from web_interface.crypto import hash_pw
        self.assertNotEqual(hash_pw("hallo"), hash_pw("hallo"))

    def test_2_hash_is_not_clear_text(self):
        from web_interface.crypto import hash_pw
        self.assertNotEqual(hash_pw("hallo"), "hallo")

    def test_time_needed_to_encrypt(self):
        # TODO: implement this to check that time is > 1 sec
        pass


class TestCheckPw(unittest.TestCase):

    def test_checking_successful(self):
        from web_interface.crypto import hash_pw, check_pw
        h1 = hash_pw("hallo")
        self.assertTrue(check_pw("hallo", h1))

    def test_checking_failing(self):
        from web_interface.crypto import hash_pw, check_pw
        h1 = hash_pw("hallo")
        self.assertFalse(check_pw("This is false", h1))


class TestTimeHashPw(unittest.TestCase):

    def test_2_different_times_should_have_different_hashes(self):
        from web_interface.crypto import time_hash
        h1 = time_hash()
        h2 = time_hash()
        self.assertNotEqual(h1, h2)

    def test_same_time_has_same_hash(self):
        from web_interface.crypto import time_hash
        import datetime
        dtime = datetime.datetime.now()
        h1 = time_hash(t=dtime)
        h2 = time_hash(t=dtime)
        self.assertEqual(h1, h2)


if __name__ == '__main__':
    unittest.main()
