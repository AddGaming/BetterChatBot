from web_interface.web_functions import log

discord_syntax = {
    "bold": "**",
    "italic": "*",
    "underline": "__",
    "crossed_out": "~~",
    "code": "`"
}
telegram_syntax = {
    "bold": "**",
    "italic": "*",
    "underline": "",
    "crossed_out": "~~",
    "code": "`"
}
youtube_syntax = {
    "bold": "",
    "italic": "",
    "underline": "",
    "crossed_out": "",
    "code": ""
}
twitch_syntax = {
    "bold": "",
    "italic": "",
    "underline": "",
    "crossed_out": "",
    "code": ""
}
html_syntax = {
    # TODO: implement? how exactly?
    "bold": "",
    "italic": "",
    "underline": "",
    "crossed_out": "",
    "code": ""
}


def switch_syntax(text: str, original, target):
    """
    This function switches the syntax-marker for a given text to change appearance in chat-apps
    :param text: the text in which to replace the markers
    :param original: a dictionary with the given markers as values and the name as key
    :param target: a dictionary adhering to the rules of 'original'
    :return: a string with replaced syntax-markers
    """
    # catching invalid params
    if not text:
        log(text="Translating Syntax-marker on an empty text", category="WARNING")
        pass
    for key in original:
        try:
            target[key]
        except KeyError:
            raise KeyError("")

    # function body
    for mark in original:
        if original[mark]:
            text = text.replace(original[mark], target[mark])
    return text
