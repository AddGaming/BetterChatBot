"""
This class manages everything with dialogflow
"""
import time

import google.cloud.dialogflow_v2 as dialogflow_vx
from .DialogFlowSettings import ALLOWED_LANGUAGES


class DialogFlowManager:

    def __init__(self, project_id="", lang_code="en"):
        if project_id == "":
            raise RuntimeError("No project ID given for DialogFlowManager")
        if lang_code not in ALLOWED_LANGUAGES:
            raise RuntimeError("Language not supported for DialogFlowManager")
        gcloud_api_key = "cloud_admin_key.json"
        self.AgentClient = dialogflow_vx.AgentsClient.from_service_account_file(gcloud_api_key)
        self.ContextClient = dialogflow_vx.ContextsClient.from_service_account_file(gcloud_api_key)
        self.EntityClient = dialogflow_vx.EntityTypesClient.from_service_account_file(gcloud_api_key)
        self.IntentClient = dialogflow_vx.IntentsClient.from_service_account_file(gcloud_api_key)
        self.SessionClient = dialogflow_vx.SessionsClient.from_service_account_file(gcloud_api_key)
        self.ProjectID = project_id
        self.ProjectPath = "projects/" + self.ProjectID
        self.AgentPath = self.AgentClient.agent_path(project=self.ProjectID)
        self.Language = lang_code

    def __str__(self):
        llist = []
        for entry in self.list_intents():
            llist.append(entry)
        r = f'linked_agents: {len(self.list_agents())}\n'
        r += f'registered_entity_types: {len(self.list_entity_types())}\n'
        r += f"registered_intents: {len(llist)}\n"
        return r

    def get_agent(self):
        """Returns the Dialogflow-Agent linked to this Project \n
        :returns: DialogFlow-Agent"""
        # since a agents is always linked to 1 project only this works as long as there is a agent registered for the
        # project
        return self.AgentClient.get_agent(parent=self.ProjectPath)

    def train_agent(self):
        """issues a training command to the linked agent to train him on the uploaded intents"""
        req = dialogflow_vx.TrainAgentRequest()
        req.parent = self.ProjectPath
        # TODO: Test the return type and include in docstring
        return self.AgentClient.train_agent(request=req)

    def list_agents_as_dict(self):
        """(This function is redundant but is here for completion sake if you put this on an older Dialogflow version)
        consider using "get_agent" instead \n
        :returns: a list of agents in my costume dict format.
        """
        ret = []
        for entry in self.AgentClient.search_agents(parent=self.ProjectPath):
            lan = [entry.default_language_code]
            for elem in entry.supported_language_codes:
                lan.append(elem)
            ret.append({"parent": entry.parent,
                        "name": entry.display_name,
                        "languages": lan,
                        "time_zone": entry.time_zone,
                        "logging?": entry.enable_logging,
                        "match_mode": entry.match_mode,
                        "classification_threshold": entry.classification_threshold})
        return ret

    def list_agents(self):
        """(This function is redundant but is here for completion sake if you put this on an older Dialogflow version)
        consider using "get_agent" instead \n
        :returns: a list of agents.
        """
        ret = []
        for entry in self.AgentClient.search_agents(parent=self.ProjectPath):
            ret.append(entry)
        return ret

    def list_entity_types(self):
        """:returns: a list of all DialogFlow-EntityTypes for the registered Agent/Project"""
        ret = []
        for elem in self.EntityClient.list_entity_types(language_code=self.Language, parent=self.AgentPath):
            ret.append(elem)
        return ret

    def list_entity_types_as_dict(self):
        """:returns: a list of all DialogFlow-EntityTypes for the registered Agent/Project in my costume
        formatted dict"""
        ret = []
        for entity_type in self.EntityClient.list_entity_types(language_code=self.Language, parent=self.AgentPath):
            str_rep = []
            for entry in entity_type.entities:
                for typ in entry.synonyms:
                    str_rep.append(typ)
            ret.append({"name": entity_type.name,
                        "display_name": entity_type.display_name,
                        "entities": str_rep})
        return ret

    def list_intents(self):
        """:returns: a list of DialogFlow-Intents"""
        return dialogflow_vx.IntentsClient.list_intents(self=self.IntentClient, parent=self.AgentPath,
                                                        language_code=self.Language)

    @staticmethod
    def create_entity_dict(name="", synonyms=None):
        """small helper to create entities in the accepted dict format
        :param name: str (name of the entity)
        :param synonyms: [] (list of all to be associated synonyms. If empty synonyms = [name])
        :returns: DialogFlow-Entity """
        if name == "":
            raise RuntimeError(f"Not all parameters sufficiently filled: \n{name=}")
        if synonyms is None:
            synonyms = [name]
        entity = {"value": name,
                  "synonyms": synonyms}
        return entity

    def create_entity_type(self, display_name="", ini_entity=None):
        """creates a new Entity-Type in the registered Agent
        :param display_name: str (the name to represent this entity [keep it ascii])
        :param ini_entity: (optional) dialogflow entity
        :returns: an instance of the created entityType """
        # sanitizing display name
        un_allowed_chars = "?:;,.(){}!\\/[]'\"#*+=-\t\näÄöÖüÜßéèêÉÈÊáàâÁÀÂ°óòôÓÒÔíìîÍÌÎúùûÚÙÛß"
        for char in un_allowed_chars:
            display_name = display_name.replace(char, "")

        # The entity_type to be added:
        new_entity_type = dialogflow_vx.EntityType()
        new_entity_type.display_name = display_name
        # dont ask me what a kind_map is. I couldn't find any information about it. But the standard intents are all
        # Kind_maps so it can't be all bad
        new_entity_type.kind = "KIND_MAP"

        if ini_entity is not None:
            new_entity_type.entities = [ini_entity]

        raw_ret = self.EntityClient.create_entity_type(parent=self.AgentPath, language_code=self.Language,
                                                       entity_type=new_entity_type)
        return raw_ret

    def create_intent(self, training_phrases=None, response="", display_name=""):
        """Creates a DialogFlow-Intent object with annotated training phrases
        :param training_phrases: [str] (a list of training phrases on which the bot should learn)
        :param response: str (the response to the questions in training_phrases)
        :param display_name: str (display name of the intent [special char's allowed])
        :returns: ?"""
        if training_phrases is None or response == "" or display_name == "":
            raise RuntimeError(f"Not all necessary parameters filled sufficiently: \n{training_phrases=}\n"
                               f"{response=}\n{display_name=}")

        my_intent = self.make_intent(training_phrases=training_phrases, response=response, display_name=display_name,
                                     entity_types_of_agent_as_dict=self.list_entity_types_as_dict())

        # upload intent
        # TODO: check return type and add to docstring
        return self.IntentClient.create_intent(parent=self.AgentPath, intent=my_intent, language_code=self.Language)

    @staticmethod
    def make_entity_type(name="", display_name=None, entities=None):
        """Creates a DialogFlow-Entity object
        :param display_name: str (the name which gets displayed)
        :param name: (optional) str (the name of an existing DialogFlow-Entity-Type)
        :param entities: (optional) [DialogFlow-Entity] (a list of entities that should belong to this Entity-Type)
        :returns: a DialogFlow-Entity object"""
        if display_name == "":
            raise RuntimeError(f"Not all necessary parameters filled sufficiently: \n{display_name=}")

        et = dialogflow_vx.EntityType()
        et.display_name = display_name
        if entities is not None:
            et.entities = entities
        if name != "":
            et.name = name
        et.kind = "KIND_MAP"
        return et

    def make_intent(self, name="", training_phrases=None, response="", display_name="",
                    entity_types_of_agent_as_dict=None):
        """Creates a DialogFlow-Intent object with annotated training phrases
        :param training_phrases: [str] (a list of training phrases on which the bot should learn)
        :param response: str (the response to the questions in training_phrases)
        :param display_name: str (display name of the intent [special char's allowed])
        :param name: (optional) str (the name of an existing intent)
        :param entity_types_of_agent_as_dict: (optional) [{}] (list of entity-type-dictionaries)
        :returns: a DialogFlow-Intent object"""
        if training_phrases is None or response == "" or display_name == "":
            raise RuntimeError(f"Not all necessary parameters filled sufficiently: \n{training_phrases=}\n"
                               f"{response=}\n{display_name=}")

        # part annotation:
        # for all sentences:
        annotated_training_phrases = []
        for sentence in training_phrases:
            # split sentence
            parts = sentence.split(" ")
            i = 0  # index
            parts_copy = parts.copy()
            un_allowed_chars = "!,?."
            for elem in parts_copy:
                for char in un_allowed_chars:
                    if len(elem) > 1:
                        if elem[-1] == char:
                            parts[i] = parts[i].replace(char, "")
                            parts.insert(i + 1, char)
                i += 1
            # get all entities
            if entity_types_of_agent_as_dict is None:
                # warning: my formatted dict, not object
                entity_types = self.list_entity_types_as_dict()
            else:
                entity_types = entity_types_of_agent_as_dict
            # check word vs entity existence
            final_phrase = []
            for part in parts:
                this_part = dialogflow_vx.types.Intent.TrainingPhrase.Part()
                this_part.text = part
                for entity_type in entity_types:
                    # if part exits in known entity-types -> annotate
                    for entity in entity_type["entities"]:
                        if part == entity:
                            this_part.entity_type = "@" + entity_type["display_name"]
                            this_part.user_defined = True
                            this_part.alias = entity_type["display_name"]
                # add to final composition
                final_phrase.append(this_part)
                # add a whitespace after:
                white_part = dialogflow_vx.types.Intent.TrainingPhrase.Part()
                white_part.text = " "
                final_phrase.append(white_part)

            # the part annotation should get highlighted wrong. How do I know?
            # Google uses it like this in their examples
            annotated_training_phrases.append(
                dialogflow_vx.Intent.TrainingPhrase(parts=final_phrase,
                                                    type_=dialogflow_vx.types.Intent.TrainingPhrase.Type.EXAMPLE))

        # construct intent
        my_intent = dialogflow_vx.Intent()
        my_intent.webhook_state = my_intent.WebhookState(0)  # ENABLED = 1 | FOR_SLOT_FILLING = 2 | UNSPECIFIED = 0
        my_intent.display_name = display_name
        if name != "":
            my_intent.name = name
        # same as in part annotation. Ignore errors thrown here by IDE, it is valid and works
        text = dialogflow_vx.Intent.Message.Text(text=[response])
        my_intent.messages = [dialogflow_vx.Intent.Message(text=text)]
        my_intent.training_phrases = annotated_training_phrases

        return my_intent

    def batch_update_entity_type(self, entity_types=None):
        """:param entity_types: [DialogFlow-Entity-Type]\n
        :returns: ?"""
        # TODO: test what gets returned
        if entity_types is None:
            raise RuntimeError(f"Not all necessary parameters filled sufficiently: \n{entity_types=}")

        entity_type_batch = dialogflow_vx.EntityTypeBatch()
        entity_type_batch.entity_types = entity_types

        req_data = dialogflow_vx.BatchUpdateEntityTypesRequest()
        req_data.entity_type_batch_inline = entity_type_batch
        req_data.language_code = self.Language
        req_data.parent = self.AgentPath
        return self.EntityClient.batch_update_entity_types(request=req_data)

    def batch_update_intents(self, intent_list=None):
        """:param: intent_list -> [DialogFlow-Intent] \n
        :returns: ?"""
        if intent_list is None:
            raise RuntimeError(f"Not all necessary parameters filled sufficiently: \n{intent_list=}")
        # TODO: test what gets returned

        intent_batch = dialogflow_vx.IntentBatch()
        intent_batch.intents = intent_list

        update_request = dialogflow_vx.BatchUpdateIntentsRequest()
        update_request.language_code = self.Language
        update_request.intent_batch_inline = intent_batch
        update_request.parent = self.AgentPath

        return self.IntentClient.batch_update_intents(request=update_request)

    def update_entity_type(self, entity_type_name="", entity_type_display_name="", entities=None):
        """updates the DialogFlow-Entity-Type that fits the given parameters \n
        :param entity_type_name: str (name of the Entity-Type) \n
        :param entity_type_name: str (display name of the Entity-Type)\n
        :param entities: list of DialogFlow-Entities \n
        :returns: the new entity_type_obj"""
        if entity_type_name == "" or entity_type_display_name == "":
            raise RuntimeError(f"Not all necessary parameters filled sufficiently: \n {entity_type_display_name=}\n"
                               f"{entity_type_name=}")
        if entities is None:
            entities = []

        updated_entity_type = dialogflow_vx.EntityType()
        updated_entity_type.display_name = entity_type_display_name
        updated_entity_type.name = entity_type_name
        updated_entity_type.kind = "KIND_MAP"
        updated_entity_type.entities = entities
        return self.EntityClient.update_entity_type(entity_type=updated_entity_type, language_code=self.Language)

    def respond(self, inpt="", session_id=None, fire_store_manager=None):
        """
        gets the DialogFlow-Response to a given input
        :param inpt: str (the input)
        :param session_id: str (a session id under which the conversation is held)
        :param fire_store_manager: FireStoreManager (a manager under which to call the respective client)
        :returns: str
        """
        if fire_store_manager is None:
            raise RuntimeError(f"Not all necessary arguments filled: {fire_store_manager=}")
        if session_id is None:
            c_time = str(time.time())
            session_id = abs(int(hash(c_time)))
        session_path = self.AgentClient.agent_path("nennig-g1-jefj") + "/sessions/" + str(session_id)

        text_input = dialogflow_vx.TextInput(text=inpt, language_code="en")
        query_input = dialogflow_vx.QueryInput(text=text_input)
        req = dialogflow_vx.DetectIntentRequest(session=session_path, query_input=query_input)
        resp = self.SessionClient.detect_intent(request=req)
        formatted_resp = resp.query_result.fulfillment_text

        # the matching of question/answer for automatic adding to db
        if "Answers" in fire_store_manager.cashed_collections:
            raw_answers_db = fire_store_manager.cashed_collections["Answers"]
        else:
            raw_answers_db = fire_store_manager.get_collection("Answers")

        word_list = fire_store_manager.list_entries(collection="Words", field_name="word")
        answers_list = fire_store_manager.list_entries(collection="Answers", field_name="answer_text")
        questions_list = fire_store_manager.list_entries(collection="Questions", field_name="question_text")
        new_questions_list = fire_store_manager.list_entries(collection="New_Questions", field_name="question_text")

        if formatted_resp in answers_list:
            answer_id = None  # against ide errors, always set.
            for key in raw_answers_db:
                sub_dic = raw_answers_db[key]
                if sub_dic["answer_text"] == formatted_resp:
                    answer_id = sub_dic["ID"]
            if inpt not in questions_list and inpt not in new_questions_list:
                doc_id = str(hash(inpt))
                up_data = {'question_text': inpt, 'answer_id': answer_id, 'context': "", 'status': "uncategorized"}
                # adding in cash and online so I dont have to pull the whole thing again
                x = fire_store_manager.cashed_collections["New_Questions"]
                fire_store_manager.add_entry_to_collection(doc_id=doc_id, collection="New_Questions", data=up_data)
                x[doc_id] = up_data
                fire_store_manager.cashed_collections["New_Questions"] = x

                un_allowed_chars = "?:;,.()!\\/[]'\"#*+-\t\n"
                new_inpt = inpt
                for char in un_allowed_chars:
                    new_inpt = new_inpt.replace(char, "")
                split_input = new_inpt.split(" ")
                for word in split_input:
                    if word not in word_list:
                        up_data = {"entity": "", "status": "unidentified", "word": word}
                        # adding in cash and online so I dont have to pull the whole thing again
                        x = fire_store_manager.cashed_collections["Words"]
                        fire_store_manager.add_entry_to_collection(doc_id=word, collection="Words", data=up_data)
                        x[word] = up_data
                        fire_store_manager.cashed_collections["Words"] = x

        else:
            if inpt not in new_questions_list:
                doc_id = str(hash(inpt))
                up_data = {'question_text': inpt, 'answer_id': "None", 'context': "", 'status': "uncategorized"}
                # adding in cash and online so I dont have to pull the whole thing again
                x = fire_store_manager.cashed_collections["New_Questions"]
                fire_store_manager.add_entry_to_collection(doc_id=doc_id, collection="New_Questions", data=up_data)
                x[doc_id] = up_data
                fire_store_manager.cashed_collections["New_Questions"] = x

        return formatted_resp

    def upstream_entities_form_table(raw_request_data=None, g_api_manager=None):
        """helper function to upload all the edited entities into the database
        :param raw_request_data: [[str,str,str,str]] (the return/post from the html-js-function)
        :param g_api_manager: GAPIManager (-under which to operate)
        :returns: None"""
        if g_api_manager is None:
            raise RuntimeError(f"Not all necessary arguments filled: {g_api_manager=}")
        if raw_request_data is None:
            raise RuntimeError("no data given to src.utils.upstream_entities_form_table")

        entity_type_list = g_api_manager.DialogFlowManager.list_entity_types()
        to_update = []

        for row_data in raw_request_data:
            if " " in row_data[0]:
                n_word = row_data[0].replace(" ", "_")
            else:
                n_word = row_data[0]

            word_data = {"word": row_data[0],
                         "status": row_data[1]}
            if row_data[1] == "ignore":
                word_data["entity"] = ""
                g_api_manager.FireStoreManager.add_entry_to_collection(doc_id=n_word, collection="Words",
                                                                       data=word_data)
            elif row_data[1] == "unidentified":
                """normally a waste of calls_per_day, BUT there is the case where data comes from dialogflow in which case 
                it IS necessary"""
                word_data["entity"] = ""
                g_api_manager.FireStoreManager.add_entry_to_collection(doc_id=n_word, collection="Words",
                                                                       data=word_data)
            elif row_data[1] == "categorized":
                current_entity_type = None  # vs compiler warning - will always be initialized later
                # finding the entity_type_name:
                for entity_type in entity_type_list:
                    # print("for_current_entity_type: " + str(entity_type))
                    if entity_type.display_name == row_data[2]:
                        current_entity_type = entity_type
                        word_data["entity"] = entity_type.name
                        row_data[3] = entity_type.name
                        break

                if row_data[3] == "":  # this meaning no existing entity_type that fits the display_name given was found
                    # entity_types can't be batch created....
                    time.sleep(1.1)  # request limit from google
                    current_entity_type = g_api_manager.DialogFlowManager.create_entity_type(display_name=row_data[2])
                    row_data[3] = current_entity_type.name
                    word_data["entity"] = row_data[3]

                try:
                    my_entity_dict = g_api_manager.DialogFlowManager.create_entity_dict(name=row_data[0],
                                                                                        synonyms=[row_data[0]])
                    entity_list = []
                    for entry in current_entity_type.entities:
                        entity_list.append(g_api_manager.DialogFlowManager.create_entity_dict(name=entry.value,
                                                                                              synonyms=entry.synonyms))
                    entity_list.append(my_entity_dict)
                except:
                    """the above only fails if the entity_type is new and therefore has no entities associated.
                    This throws a google specific error which I dont know how to catch"""
                    entity_list = [g_api_manager.DialogFlowManager.create_entity_dict(name=row_data[0],
                                                                                      synonyms=[row_data[0]])]
                g_api_manager.FireStoreManager.add_entry_to_collection(doc_id=n_word, collection="Words",
                                                                       data=word_data)

                temp_entity_type = g_api_manager.DialogFlowManager.make_entity_type(entity_type_name=row_data[3],
                                                                                    entity_type_display_name=row_data[
                                                                                        2],
                                                                                    entities=entity_list)
                doupe_found = False
                for entity_type in to_update:
                    if entity_type["name"] == temp_entity_type["name"]:
                        for entity in temp_entity_type["entities"]:
                            if entity not in entity_type["entities"]:
                                entity_type["entities"].append(entity)
                        doupe_found = True

                if not doupe_found:
                    to_update.append(temp_entity_type)
                entity_type_list = g_api_manager.DialogFlowManager.list_entity_types()

            else:
                return RuntimeError(f"Input has unexpected property: \nin request_data:\n{row_data=}")

        g_api_manager.DialogFlowManager.batch_update_entity_type(entity_types=to_update)
        upstream_intents_from_db(g_api_manager)  # updating the training base of the Dialogflow bot
        g_api_manager.FireStoreManager.get_collection("Words")  # get the online changes in the cash

    def upstream_intents_from_db(g_api_manager=None):
        """helper function to generate and edit the intents from the database to DialogFlow
        :param g_api_manager: GAPIManager (-under which to operate)
        :returns: None"""
        # UNDER BUGFIX CONSTRUCTION
        if g_api_manager is None:
            raise RuntimeError(f"Not all necessary arguments filled: {g_api_manager=}")

        if "Answers" in g_api_manager.FireStoreManager.cashed_collections:
            responses = g_api_manager.FireStoreManager.cashed_collections["Answers"]
        else:
            responses = g_api_manager.FireStoreManager.get_collection("Answers")
        if "Questions" in g_api_manager.FireStoreManager.cashed_collections:
            questions = g_api_manager.FireStoreManager.cashed_collections["Questions"]
        else:
            questions = g_api_manager.FireStoreManager.get_collection("Questions")

        raw_intents = g_api_manager.DialogFlowManager.list_intents()
        existing_intent_answers = []
        for raw_intent in raw_intents:
            existing_intent_answers.append(raw_intent.messages[0].text.text[0])

        # create map
        response_map = {}
        for key in responses:
            llist = {}
            for kkey in questions:
                sub_dic = questions[kkey]
                if str(sub_dic['answer_id']) == key:
                    # TODO: BUGFIX THIS
                    print(f'{sub_dic["question_text"]=}')
                    llist.update({sub_dic['question_text']})
            answer_entry = responses[key]
            response_map[key] = {"response": answer_entry["answer_text"],
                                 "questions": llist}

        # % step for user feedback
        current_progress = 0
        step_size = 100 / len(response_map)

        to_update = []

        entity_type_dict = g_api_manager.DialogFlowManager.list_entity_types_as_dict()

        """for key in response_map:
            print(f'{response_map[key]}')
        existing_intent_answers.sort()"""
        for elem in existing_intent_answers:
            print(f'{elem=}')
        for key in response_map:
            entry = response_map[key]
            # print(f'{entry["response"]}')
            if entry["response"] in existing_intent_answers:
                for raw_intent in raw_intents:
                    if raw_intent.messages[0].text.text[0] == entry["response"]:
                        i_name = raw_intent.name
                print(f'{i_name=} | {entry["response"]=}')
                to_update.append(g_api_manager.DialogFlowManager.make_intent(name=i_name,
                                                                             training_phrases=entry["questions"],
                                                                             response=entry["response"],
                                                                             display_name="autogen_" + key,
                                                                             entity_types_of_agent_as_dict=entity_type_dict))
            else:
                # there is no batch create for intents...
                time.sleep(1.1)  # welp, the timeout is back
                print(f'{entry["questions"]=} | {entry["response"]=}')
                g_api_manager.DialogFlowManager.create_intent(training_phrases=entry["questions"],
                                                              response=entry["response"], display_name="autogen_" + key)

            current_progress += step_size
            if round(current_progress) < 100:
                print("\r" + str(round(current_progress)) + "%", end='')
            else:
                print("\r" + str(100) + "%")

        g_api_manager.DialogFlowManager.batch_update_intents(intent_list=to_update)
        g_api_manager.DialogFlowManager.train_df_agent()  # updating the agent that he works with the new information

    def upstream_questions_form_table(raw_request_data=None, g_api_manager=None):
        """helper function to upload all the edited questions into the database
            :param raw_request_data: [[str,str,str,int,str]] (the return/post from the html-js-function)
            :param g_api_manager: GAPIManager (-under which to operate)
            :returns: None"""
        if g_api_manager is None:
            raise RuntimeError(f"Not all necessary arguments filled: {g_api_manager=}")

        if "Answers" in g_api_manager.FireStoreManager.cashed_collections:
            raw_answer_db = g_api_manager.FireStoreManager.cashed_collections["Answers"]
        else:
            raw_answer_db = g_api_manager.FireStoreManager.get_collection("Answers")
        if "Questions" in g_api_manager.FireStoreManager.cashed_collections:
            raw_question_db = g_api_manager.FireStoreManager.cashed_collections["Questions"]
        else:
            raw_question_db = g_api_manager.FireStoreManager.get_collection("Questions")

        free_id_found = False
        free_id = 0
        # determine new id distribution starting value
        while not free_id_found:
            if "n_" + str(free_id) not in raw_question_db:
                free_id_found = True
            else:
                free_id += 1

        # [['-8169639638921596628', 'Was wurden für Waffen benutzt?', 'unidentified', 38, 'Die Kontahenten...'],...]
        for data_entry in raw_request_data:
            if data_entry[2] == 'ignore':
                nq_data = {"answer_id": "",
                           "context": "",
                           "question_text": data_entry[1],
                           "status": data_entry[2]}
                g_api_manager.FireStoreManager.add_entry_to_collection(doc_id=data_entry[0], collection="New_Questions",
                                                                       data=nq_data)
            elif data_entry[2] == 'categorized':
                nq_data = {"answer_id": int(data_entry[3]),
                           "context": "",
                           "question_text": data_entry[1]}
                n_id = "n_" + str(free_id)
                g_api_manager.FireStoreManager.add_entry_to_collection(doc_id=n_id, collection="Questions",
                                                                       data=nq_data)

                if str(nq_data["answer_id"]) not in raw_answer_db:
                    na_data = {"ID": nq_data["answer_id"],
                               "answer_text": data_entry[4],
                               "audio_id": 0}
                    g_api_manager.FireStoreManager.add_entry_to_collection(doc_id=str(nq_data["answer_id"]),
                                                                           collection="Answers", data=na_data)

                # remove from new_question collection
                g_api_manager.FireStoreManager.delete_entry_from_collection(collection="New_Questions",
                                                                            doc_id=data_entry[0])

                free_id += 1

            # else: -> unidentified -> do nothing

        update_wordlist_form_db(g_api_manager)
        upstream_intents_from_db(g_api_manager)
        g_api_manager.FireStoreManager.get_collection("Questions")
        g_api_manager.FireStoreManager.get_collection("Answers")
        g_api_manager.FireStoreManager.get_collection("New_Questions")
        g_api_manager.FireStoreManager.get_collection("Words")
