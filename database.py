import datetime
from os import path

from flask import flash
from sqlalchemy import asc, desc


def create_database(flask_app=None, db=None, db_name=""):
    if not flask_app or not db or not db_name:
        raise AttributeError(f"You passed in a empty argument:\n{flask_app=} | {db=} | {db_name=}")

    if not path.exists("website/" + db_name):
        db.create_all(app=flask_app)
        from web_interface.web_functions import log
        # log("Created database!")


def init_db(db=None):
    # local import because db creation must precede
    from db_models import Ott, Application, Platform, Group, GoogleSetting
    from web_interface.web_functions import log
    from web_interface.crypto import time_hash
    if not Ott.query.get(1):
        ott = Ott(value="1")  # default first entry
        db.session.add(ott)
        for i in range(19):
            db.session.add(Ott(value=time_hash()))
        log(text="Generated full list of OTTs", category="Status")
    if not Application.query.get(1):
        app = Application()
        db.session.add(app)
        log(text="Created Application in Database", category="Status")
    if not Platform.query.all():
        discord = Platform(name="discord")
        telegram = Platform(name="telegram")
        youtube = Platform(name="youtube")
        twitch = Platform(name="twitch")
        system = Platform(name="system", status="online")
        db.session.add(discord)
        db.session.add(telegram)
        db.session.add(youtube)
        db.session.add(twitch)
        db.session.add(system)
        log(text="Created Platforms in Database", category="Status")
    if not Group.query.all():
        disabled = Group(name="disabled")
        db.session.add(disabled)
        log(text="Initialized Groups", category="Status")
    if not GoogleSetting.query.all():
        cloud = GoogleSetting()
        db.session.add(cloud)
        log(text="Initialized GoogleSettings", category="Status")

    db.session.commit()


def top_commands_3(db=None):
    if db is None:
        raise AttributeError("please pass in a valid Database, thx")
    from db_models import Command
    descending = db.session.query(Command).order_by(desc(Command.usages))
    if not descending:
        return [("nan", 0), ("nan", 0), ("nan", 0)]
    llist = []
    for i in range(3):
        try:
            llist.append((descending[i].name, descending[i].usages))
        except IndexError:
            llist.append(("nan", 0))
    return llist


def flop_commands_3(db=None):
    if db is None:
        raise AttributeError("please pass in a valid Database, thx")
    from db_models import Command
    ascending = db.session.query(Command).order_by(asc(Command.usages))
    if not ascending:
        return [("nan", 0), ("nan", 0), ("nan", 0)]
    llist = []
    for i in range(3):
        try:
            llist.append((ascending[i].name, ascending[i].usages))
        except IndexError:
            llist.append(("nan", 0))
    return llist


def call_analysis():
    from db_models import Platform
    platforms = Platform.query.all()
    ret = {
        "total": 0, "week": 0, "today": 0, "total_commands": 0, "week_commands": 0, "today_commands": 0,
        "total_nlu": 0, "week_nlu": 0, "today_nlu": 0
    }
    for platform in platforms:
        today = (platform.n_commands_today, platform.n_nlu_today)
        week = (platform.n_commands_week, platform.n_nlu_week)
        total = (platform.n_commands_total, platform.n_nlu_total)
        ret[platform.name] = [today, week, total, None]
        ret["total"] += total[0] + today[1]
        ret["week"] += week[0] + week[1]
        ret["today"] += today[0] + today[1]
        ret["total_commands"] += total[0]
        ret["week_commands"] += week[0]
        ret["today_commands"] += today[0]
        ret["total_nlu"] += today[1]
        ret["week_nlu"] += week[1]
        ret["today_nlu"] += today[1]
    for platform in platforms:
        try:
            percent = (ret[platform.name][0][0] + ret[platform.name][0][1]) / (ret["total"] / 100)
        except ZeroDivisionError:
            percent = 0
        ret[platform.name][3] = percent
    return ret


def parse_csv_to_commands(csv="", db=None):
    """
    takes in a string representing a csv (from the web forms) and parses it to commands into the database
    :param csv: (str)
    :param db: the database object ot perform the writing on
    :return: none
    """
    if db is None:
        raise AttributeError("please pass in a valid Database, thx")
    from db_models import Command, Group, User
    from flask_login import current_user
    from web_interface.web_functions import log
    csv = csv.split("\n")
    changes = 0
    try:
        for line in csv:
            n_line = line.split(",")
            cmd = Command.query.filter_by(name=n_line[0]).first()
            if cmd:  # if the command already exists, replace ("update") it
                log(text=f"The command({cmd.name}) in CSV already exists -> updating existing one", category="WARNING")
                Command.query.filter_by(id=cmd.id).delete()
            if not Group.query.filter_by(name=n_line[2]).first():
                db.session.add(Group(name=n_line[2]))
                log(text=f"The mentioned group({n_line[2]}) in the command({n_line[0]}) doesn't exist -> "
                         f"creating this group",
                    category="WARNING")
            new_cmd = Command(
                name=n_line[0],
                symbol=n_line[1],
                group=Group.query.filter_by(name=n_line[2]).first().id,
                author=current_user.id,
                date=datetime.datetime.now(),
                usages=int(n_line[3]),
                active=(n_line[4].lower() == "true"),
                text_discord=parse_raw_command_text(n_line[5], "discord"),
                text_telegram=parse_raw_command_text(n_line[6], "telegram"),
                text_youtube=parse_raw_command_text(n_line[7], "youtube"),
                text_twitch=parse_raw_command_text(n_line[8], "twitch"),
                auto_delete=(n_line[9].lower() == "true"),
                auto_delete_time=int(n_line[10]))
            db.session.add(new_cmd)
            changes += 1
    except IndexError:
        log(text="The passed in CSV is badly formatted!", category="ERROR")
        flash("An error occurred while adding the commands to the database. For more information visit the logs",
              "error")
        return
    User.query.filter_by(id=current_user.id).first().changes += changes
    db.session.commit()
    log(text="Finished importing the CSV commands", category="STATUS")
    flash("Successfully added Commands to Database", "success")


def parse_raw_command_text(text=None, platform=""):
    """
    This function converts a command string into the wanted output.
    The command substitution logic is handled here.
    :param text: the message as string
    :param platform: the platform name as string
    :return: the output-ready text to be displayed to users
    """
    if text is None or not platform:
        raise ValueError(f"Your text or platform is invalid\n{text=}\n{platform=}")
    from db_models import Command
    if platform.lower() == "discord":
        for command in Command.query.all():
            text = text.replace((command.symbol + command.name), command.text_discord)
        return text
    elif platform.lower() == "telegram":
        for command in Command.query.all():
            text = text.replace((command.symbol + command.name), command.text_telegram)
        return text
    elif platform.lower() == "youtube":
        for command in Command.query.all():
            text = text.replace((command.symbol + command.name), command.text_youtube)
        return text
    elif platform.lower() == "twitch":
        for command in Command.query.all():
            text = text.replace((command.symbol + command.name), command.text_twitch)
        return text
    else:
        raise ValueError(f"Your entered platform is not supported: {platform=}")


def update_command(data=[], db=None):
    """
    updates a command in the database
    :param data:
    :param db:
    :return:
    """
    # catching invalid inputs
    from web_interface.web_functions import log
    if db is None:
        raise AttributeError("please pass in a valid Database, thx")
    if len(data) != 10:
        log(text=f"The command data has the wrong amount of fields. \n expected: 10\n received: {len(data)}",
            category="ERROR")
        return
    from db_models import Command, Group, User
    if not Command.query.filter_by(name=data[0]).first():
        log(text=f"The command you want to update ('{data[0]}') doesn't exists", category="ERROR")
        return

    # command body
    from flask_login import current_user
    usages = Command.query.filter_by(name=data[0]).first().usages
    old_id = Command.query.filter_by(name=data[0]).first().id
    old_time = Command.query.filter_by(name=data[0]).first().auto_delete_time
    Command.query.filter_by(name=data[0]).delete()  # deleting the old one
    if data[9] is None:
        data[9] = old_time
    new_texts = cascade_command_text(data[4], data[5], data[6], data[7])
    new_cmd = Command(
        id=old_id,
        name=data[0],
        symbol=data[1],
        group=Group.query.filter_by(id=data[2]).first().id,
        author=current_user.id,
        date=datetime.datetime.now(),
        usages=usages,
        active=(data[3]),
        text_discord=new_texts[0],
        text_telegram=new_texts[1],
        text_youtube=new_texts[2],
        text_twitch=new_texts[3],
        auto_delete=bool(data[8]),
        auto_delete_time=int(data[9])
    )
    db.session.add(new_cmd)
    User.query.filter_by(id=current_user.id).first().changes += 1
    db.session.commit()


def update_nlu_answer(data=[], db=None):
    """
    updates the answer text in the nlu related database
    :param data: [old answer text: str, new answer text: str]
    :param db: the database to operate on
    :return: None
    """
    # catching invalid inputs
    if db is None:
        raise AttributeError("please pass in a valid Database, thx")
    if len(data) != 2:
        raise IndexError(f"The given data array has {len(data)} length.\n expected length: 2")

    # function body
    from db_models import Answer, User
    from flask_login import current_user

    old_id = Answer.query.filter_by(text=data[0]).first().id
    Answer.query.filter_by(text=data[0]).delete()
    new_answer = Answer(
        id=old_id,
        text=data[1],
        author=current_user.id)
    db.session.add(new_answer)
    User.query.filter_by(id=current_user.id).first().changes += 1
    db.session.commit()
    from web_interface.web_functions import log
    log(text=f"updated answer id {old_id}", category="STATUS")


def update_nlu_question(data=[], db=None):
    """
    updates the question text in the nlu related database
    :param data: [old question text: str, new question text: str, new q status: str, new q answer id: int]
    :param db: the database to operate on
    :return: None
    """
    # TODO: update the structure that way you only have to put the new parameters in without the old
    # catching invalid inputs
    if db is None:
        raise AttributeError("please pass in a valid Database, thx")
    if len(data) != 4:
        raise IndexError(f"The given data array has {len(data)} length.\n expected length: 4")

    # function body
    from db_models import Question, User
    from flask_login import current_user

    old_id = Question.query.filter_by(text=data[0]).first().id
    Question.query.filter_by(text=data[0]).delete()
    new_question = Question(
        id=old_id,
        text=data[1],
        status=data[2],
        answer=int(data[3]))
    db.session.add(new_question)
    User.query.filter_by(id=current_user.id).first().changes += 1
    db.session.commit()
    from web_interface.web_functions import log
    log(text=f"updated question id {old_id}", category="STATUS")


def update_nlu_entity(data=[], db=None):
    """
    Updates the given parameters of the entity. If you dont want to update a certain param enter a None as placeholder.
    examples: [a,b,c,d] // [a,None,b,None]
    :param data: [old_name : str, new_name : str, old_ulr : str, new_url : str]
    :param db: the database to operate on
    :return: None
    """
    if db is None:
        raise AttributeError("please pass in a valid Database, thx")
    if len(data) != 4:
        raise IndexError(f"The given data array has {len(data)} length.\n expected length: 4")
    from db_models import Entity, User
    from flask_login import current_user

    if data[0]:
        old_entity = Entity.query.filter_by(name=data[0]).first()
    else:
        old_entity = Entity.query.filter_by(ulr=data[2]).first()
    if not data[1]:
        data[1] = data[0]
    if not data[3]:
        data[3] = data[2]

    Entity.query.filter_by(name=data[0]).delete()  # deleting the old one
    new_entity = Entity(
        id=old_entity.id,
        name=data[1],
        url=data[3])
    db.session.add(new_entity)
    User.query.filter_by(id=current_user.id).first().changes += 1
    db.session.commit()


def update_nlu_word(data=[], db=None):
    """
    Updates a word for nlu in the database. if you dont want to update a field, leave it empty
    :param data:
    :param db:
    :return:
    """
    # TODO: continue from here
    if db is None:
        raise AttributeError("please pass in a valid Database, thx")
    from db_models import Word, User
    from flask_login import current_user
    old_id = Word.query.filter_by(text=data[0]).first().id
    Word.query.filter_by(text=data[0]).delete()  # deleting the old one
    # TODO: better error report
    new_word = Word(
        id=old_id,
        text=data[0],
        status=data[1],
        entity=int(data[2]))
    db.session.add(new_word)
    User.query.filter_by(id=current_user.id).first().changes += 1
    db.session.commit()


def parse_csv_to_answers(csv="", db=None, separator="|"):
    """
    takes a scv string and turns it into answers written into the database
    :param csv: a csv string
    :param db: the database to write to
    :param separator: csv is separated by "|" to enable "," in texts by default.
    :return: none
    """
    if db is None:
        raise AttributeError("please pass in a valid Database, thx")
    from db_models import Answer, User
    from flask_login import current_user
    csv = csv.replace("\r", "").split("\n")
    for line in csv:
        n_line = line.split(separator)
        raw_answer = Answer.query.filter_by(text=n_line[0]).first()
        if raw_answer:  # if the command already exists, replace ("update") it
            Answer.query.filter_by(id=raw_answer.id).delete()
        # TODO: better error report
        new_answer = Answer(text=n_line[0], author=current_user.id)
        db.session.add(new_answer)
        User.query.filter_by(id=current_user.id).first().changes += 1
    db.session.commit()


def parse_csv_to_questions(csv="", db=None, separator="|"):
    """csv is separated by "|" to enable "," in texts"""
    if db is None:
        raise AttributeError("please pass in a valid Database, thx")
    from db_models import Question, User, Application
    from flask_login import current_user
    print(f"{csv=}")
    csv = csv.replace("\r", "").split("\n")
    print(f"{csv=}")
    for line in csv:
        n_line = line.split(separator)
        print(f"{n_line=}")
        raw_question = Question.query.filter_by(text=n_line[0]).first()
        if raw_question:  # if the command already exists, replace ("update") it
            Question.query.filter_by(id=raw_question.id).delete()
        # TODO: better error report
        try:
            n_line[2] = int(n_line[2])
        except (TypeError, ValueError):
            n_line[2] = None
        new_question = Question(text=n_line[0], status=n_line[1], answer=n_line[2])
        db.session.add(new_question)
        User.query.filter_by(id=current_user.id).first().changes += 1
        if n_line[1] == "to_do" or n_line[1] == "uncategorized":
            Application.query.filter_by(id=1).first().nlu_to_do_intents += 1
    db.session.commit()


def parse_csv_to_entities(csv="", db=None, separator="|"):
    # TODO: try to factorize those repetitive functions
    """csv is separated by "|" to enable "," in texts"""
    if db is None:
        raise AttributeError("please pass in a valid Database, thx")
    from db_models import Entity, User
    from flask_login import current_user
    csv = csv.replace("\r", "").split("\n")
    for line in csv:
        n_line = line.split(separator)
        raw_entity = Entity.query.filter_by(name=n_line[0]).first()
        if raw_entity:  # if the command already exists, replace ("update") it
            Entity.query.filter_by(id=raw_entity.id).delete()
        # TODO: better error report
        new_entity = Entity(name=n_line[0], url=n_line[1])
        db.session.add(new_entity)
        User.query.filter_by(id=current_user.id).first().changes += 1
    db.session.commit()


def parse_csv_to_words(csv="", db=None, separator="|"):
    """csv is separated by "|" to enable "," in texts"""
    if db is None:
        raise AttributeError("please pass in a valid Database, thx")
    from db_models import Word, User, Application
    from flask_login import current_user
    csv = csv.replace("\r", "").split("\n")
    for line in csv:
        n_line = line.split(separator)
        raw_entity = Word.query.filter_by(text=n_line[0]).first()
        if raw_entity:  # if the command already exists, replace ("update") it
            Word.query.filter_by(id=raw_entity.id).delete()
        # TODO: better error report
        try:
            n_line[2] = int(n_line[2])
        except ValueError:
            n_line[2] = None
        new_word = Word(text=n_line[0], status=n_line[1], entity=n_line[2])
        db.session.add(new_word)
        User.query.filter_by(id=current_user.id).first().changes += 1
        if n_line[1] == "to_do" or n_line[1] == "uncategorized":
            Application.query.filter_by(id=1).first().nlu_to_do_entities += 1
    db.session.commit()


def update_application(db=None):
    if db is None:
        raise AttributeError("please pass in a valid Database, thx")
    from db_models import Word, Application, Question, Answer, Entity
    new_words = []
    all_words = []
    for w in Word.query.all():
        if w.status == "to_do" or w.status == "uncategorized":
            new_words.append(w)
        all_words.append(w)
    Application.query.filter_by(id=1).first().nlu_to_do_entities = len(new_words)
    Application.query.filter_by(id=1).first().nlu_word_count = len(all_words)

    new_questions = []
    training_questions = []
    for q in Question.query.all():
        if q.status == "to_do" or q.status == "uncategorized":
            new_questions.append(q)
        elif q.status == "categorized":
            training_questions.append(q)

    Application.query.filter_by(id=1).first().nlu_to_do_intents = len(new_questions)
    Application.query.filter_by(id=1).first().nlu_training_count = len(training_questions)

    all_answers = []
    for a in Answer.query.all():
        all_answers.append(a)
    Application.query.filter_by(id=1).first().nlu_intent_count = len(all_answers)

    all_entities = []
    for e in Entity.query.all():
        all_entities.append(e)
    Application.query.filter_by(id=1).first().nlu_entity_count = len(all_entities)

    db.session.commit()


def write_service_account_file(db=None):
    if db is None:
        raise AttributeError("please pass in a valid Database, thx")
    from db_models import GoogleSetting
    settings = GoogleSetting.query.filter_by(id=1).first()
    text = [None, None, None, None, None, None, None, None, None, None, None, None]

    f_p_key = settings.private_key.replace("\n", "\\n")
    print(f_p_key)

    text[0] = "{"
    text[1] = f'  "type": "{settings.type}",'
    text[2] = f'  "project_id": "{settings.project_id}",'
    text[3] = f'  "private_key_id": "{settings.private_key_id}",'
    text[4] = f'  "private_key": "{f_p_key}",'
    text[5] = f'  "client_email": "{settings.client_mail}",'
    text[6] = f'  "client_id": "{settings.client_id}",'
    text[7] = f'  "auth_uri": "{settings.auth_uri}",'
    text[8] = f'  "token_uri": "{settings.token_uri}",'
    text[9] = f'  "auth_provider_x509_cert_url": "{settings.auth_provider_x509_cert_url}",'
    text[10] = f'  "client_x509_cert_url": "{settings.client_x509_cert_url}"'
    text[11] = "}"
    with open("cloud_admin_key.json", 'w') as f:
        for e in text:
            f.write(e + "\n")


def update_google_settings(data=[], db=None):
    if db is None:
        raise AttributeError("please pass in a valid Database, thx")
    from db_models import GoogleSetting
    print(data)

    GoogleSetting.query.filter_by(id=1).delete()
    new_settings = GoogleSetting(
        id=1,
        connected=True,
        type=data[0],
        project_id=data[1],
        private_key_id=data[2],
        private_key=data[3],
        client_mail=data[4],
        client_id=data[5],
        auth_uri=data[6],
        token_uri=data[7],
        auth_provider_x509_cert_url=data[8],
        client_x509_cert_url=data[9]
    )
    db.session.add(new_settings)
    db.session.commit()
    write_service_account_file(db)


def cascade_command_text(discord: str, telegram: str, youtube: str, twitch: str):
    """
    This function takes all the platforms command messages as strings and then fills in the empty slots
    :param discord: message text as string
    :param telegram: message text as string
    :param youtube: message text as string
    :param twitch: message text as string
    :return: a list of all the messages-strings in the order that they got passed in
    """
    # TODO: increase abstraction and flexibility of this function
    from Utils.str_format_transform import discord_syntax, telegram_syntax, switch_syntax, youtube_syntax, twitch_syntax
    llist = [discord, telegram, youtube, twitch]
    syntax = [discord_syntax, telegram_syntax, youtube_syntax, twitch_syntax]
    for i in range(len(llist)):
        if llist[i]:  # text at pos is given - cascade down by 1
            try:
                if not llist[i + 1]:
                    llist[i + 1] = switch_syntax(llist[i], syntax[i], syntax[i + 1])
            except IndexError:
                pass  # the last slot cant be cascaded downwards

        else:  # text at pos is not given - Cascade up
            if llist[0]:
                llist[i] = switch_syntax(llist[1], discord_syntax, syntax[i])
            elif llist[1]:
                llist[i] = switch_syntax(llist[1], telegram_syntax, syntax[i])
            elif llist[2]:
                llist[i] = switch_syntax(llist[2], youtube_syntax, syntax[i])
            elif llist[3]:
                llist[i] = switch_syntax(llist[3], twitch_syntax, syntax[i])

    return llist
